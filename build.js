#!/usr/bin/env node

var fs   = require('fs')
  , jade = require('jade')
  , path = require('path')
  , yaml = require('js-yaml');

getOldPages('./pages-static').map(buildOldPage);
getNewPages('./pages').map(buildNewPage);

function getNewPages(srcpath) {
  return fs.readdirSync(srcpath).filter(function(file) {
    return fs.statSync(path.join(srcpath, file)).isDirectory();
  })
}

function buildNewPage (page) {
  var fullpath = path.resolve(path.join('.', 'pages', page));
  var context  = yaml.safeLoad(fs.readFileSync(path.join(fullpath, 'data.yaml'), 'utf8'));
  try {
    context = require(path.join(fullpath, 'modify.js'))(context);
  } catch (e) {}
  context.pretty = true;
  var rendered = jade.renderFile(path.join(fullpath, 'template.jade'), context);
  fs.writeFileSync(path.join('.', 'output', page + '.html'), rendered);
}

function getOldPages(srcpath) {
  return fs.readdirSync(srcpath).filter(function(file) {
    return path.extname(file) === '.html';
  });
}

function buildOldPage(page) {
  var context =
    { pretty:        true
    , staticContent: fs.readFileSync(path.join('.', 'pages-static', page), 'utf8') };
  var rendered = jade.renderFile(path.join('pages', 'template.jade'), context);
  fs.writeFileSync(path.join('.', 'output', page), rendered);
}
